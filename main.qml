import QtQuick 2.0
Window {
    width: 1920; height: 1080;
    visible: true
    title: "Kahoot!!"

    Rectangle {
    id: background
    width: 1920; height: 1080
    color: "purple"
    anchors.centerIn: parent



    Grid {
        id: grid
        anchors.centerIn:parent
        rows: 2; columns: 2; spacing: 1

        Button {text: "Alternative A"}
        Button {text: "Alternative B"}
        Button {text: "Alternative C"}
        Button {text: "Alternative D"}

    }

    Question {
        x:870; y:400;
        text:"This is a question"


    }
}
}
