import QtQuick 2.0
import QtQuick.Controls

Button {
    id: alternative
    font.family: "Times New Roman"
    font.pixelSize: 18

    contentItem: Text {
            text: alternative.text
            font: alternative.font

            opacity: enabled ? 1.0 : 0.3
            color: alternative.down ? "#7CB9E8" : "#7CB9E8"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight

        }

        background: Rectangle {
            implicitWidth: 300
            implicitHeight: 80
            opacity: enabled ? 1 : 0.3
            ColorAnimation on color { to: "yellow"; duration: 1000 }
            border.color: alternative.down ? "#7CB9E8" : "#7CB9E8"
            border.width: 1
            radius: 2
        }
}
